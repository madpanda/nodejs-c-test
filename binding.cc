#include <node.h>
#include <v8.h>

using namespace v8;

void Method(const v8::FunctionCallbackInfo<v8::Value>& args) {
  Isolate* isolate = args.GetIsolate();
  args.GetReturnValue().Set(String::NewFromUtf8(isolate, "hello"));
}

void Method2(const v8::FunctionCallbackInfo<v8::Value>& args) {
  Isolate* isolate = args.GetIsolate();
  args.GetReturnValue().Set(String::NewFromUtf8(isolate, "world"));
}

void Method3(const v8::FunctionCallbackInfo<v8::Value>& args) {
  Isolate* isolate = args.GetIsolate();


  Local<Function> callback = Local<Function>::Cast(args[0]);

  const unsigned argc = 1;
  Local<Value> argv[argc] = {
    Local<Value>::New(isolate, Handle<Object>::Cast(String::NewFromUtf8(isolate, "goofed")))
  };

  callback->Call(callback, argc, argv);
}

void init(v8::Local<v8::Object> exports) {
  NODE_SET_METHOD(exports, "hello", Method);
  NODE_SET_METHOD(exports, "foo", Method2);
  NODE_SET_METHOD(exports, "callback", Method3);
}

NODE_MODULE(NODE_GYP_MODULE_NAME, init)