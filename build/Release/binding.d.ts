export function hello(): string;
export function foo(): string;
export function callback(cb: (bar: string) => void): void;